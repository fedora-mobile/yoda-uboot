Name:           pp-uboot
#
Version:        2021.10
Release:        rc3%{?dist}
Summary:        uboot (Yoda) from pine64.org with UEFI Btrfs and Crust support for the PinePhone 2GB and 3GB models and the PineTab

License:        GPLv3+
URL:            https://gitlab.com/pine64-org/u-boot
Source0:        boot.scr
Source1:        boot.cmd
Source2:        Readme.uboot
Source3:        u-boot-sunxi-with-spl.bin
Source4:        update-boot-script.sh
Source5:        update-uboot.sh

Requires:       bash

Conflicts:      yoda-uboot

%description
Uboot from https://gitlab.com/pine64-org/u-boot with support for 2GB and 3GB phones and PineTab - and with Crust suspend support.

%global debug_package %{nil}

%pre
if [ $(stat -fc %T /) == 'btrfs' ] ; then
    echo "Updating btrfs"
else
    echo "f2fs is no longer support. Upgrade your install to btrfs"
    exit 1
fi




%prep
# No prep steps


%install

mkdir -p $RPM_BUILD_ROOT/boot/
cp %{SOURCE0} $RPM_BUILD_ROOT/boot/
cp %{SOURCE1} $RPM_BUILD_ROOT/boot/
cp %{SOURCE2} $RPM_BUILD_ROOT/boot/
cp %{SOURCE3} $RPM_BUILD_ROOT/boot/
cp %{SOURCE4} $RPM_BUILD_ROOT/boot/
cp %{SOURCE5} $RPM_BUILD_ROOT/boot/


%post

cd /boot
bash ./update-uboot.sh


%files

/boot/u-boot-sunxi-with-spl.bin
/boot/update-uboot.sh
/boot/Readme.uboot
/boot/update-boot-script.sh
/boot/boot.cmd
/boot/boot.scr


%changelog
* Fri Sep 03 2021 Tor - 2021.10-rc3
- Update for btrfs

* Wed Apr 07 2021 Tor - 2021.01-rc5
- Changing the version

* Sat Apr 03 2021 Yoda - 2021.01-rc4b
- Updated ATF and Crust for kernels with new CPUIDLE support
- Included boot.cmd and boot.scr into this package

* Fri Jan 01 2021 Yoda - 2021.01-rc4
- Update to latest version. fixing wifi ip change

* Wed Oct 14 2020 Yoda - 1.0.0-0
- Initial packaging (hack)
