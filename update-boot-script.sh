#!/bin/sh

set -e -x

# Generates boot.scr that contains commands to be loaded and executed by u-boot
# cd /boot
mkimage -A arm64 -T script -C none -d boot.cmd boot.scr
