#!/bin/sh

FSTYPE=$(stat -fc %T /)

if [ $FSTYPE == 'f2fs' ]
then
	echo "f2fs is no longer supported. Please upgrade to the btrfs image."
	exit 1
else
	echo "Continue update btrfs system"
fi


set -e -x

drive=$(findmnt -M / -n -o SOURCE)
# Installs u-boot to the Allwinner's approximation of MBR
dd if=/boot/u-boot-sunxi-with-spl.bin of=${drive:0:12} bs=1024 seek=8
sync
